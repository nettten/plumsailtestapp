import React, { Component } from 'react';
import { FormErrors } from './FormErrors.js';
import UserService from './api/UserService.js';
import { Redirect } from 'react-router-dom';
import Loader from 'react-loader';
import './UserRegistrationForm.css';

const SEX = {
  MALE: '0',
  FEMALE: '1',
};

const CITIES = {
  MOSCOW: '777',
  ST_PETERSBURG: '78',
  KURSK: '46',
};

export default class UserRegistrationForm extends Component {

  constructor(props){
    super(props);

    this.state = {
      name: '',
      lastname: '',
      patronymic: '',
      login: '',
      email: '',
      sex: SEX.MALE,
      birthday: '',
      password: '',
      passwordRepeat: '',
      city: CITIES.MOSCOW,

      //for form validation
      nameValid: false,
      lastnameValid: false,
      patronymicValid: true,
      loginValid: false,
      emailValid: false,
      passwordValid: false,
      passwordRepeatValid: false,

      formValid: false,

      formErrors: {
        name: '',
        lastname: '',
        patronymic: '',
        login: '',
        email: '',
        passwordRepeat: '',
      },

      redirectToMainPage: false,
      loaderLoaded: true,
    };
  }

  componentDidMount(){
    document.title = "Регистрация";
  }

  validateForm() {
    this.setState({formValid: this.state.nameValid &&
      this.state.lastnameValid &&
      this.state.patronymicValid &&
      this.state.loginValid &&
      this.state.emailValid &&
      this.state.passwordRepeatValid &&
      this.state.passwordValid
    });
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;

    let nameValid = this.state.nameValid;
    let lastnameValid = this.state.lastnameValid;
    let patronymicValid = this.state.patronymicValid;
    let emailValid = this.state.emailValid;
    let loginValid = this.state.loginValid;
    let passwordRepeatValid = this.state.passwordRepeatValid;
    let passwordValid = this.state.passwordValid;

    switch(fieldName) {
      case 'name':
        nameValid = value.length <= 64;
        fieldValidationErrors.name = nameValid ? '' : 'Максимально допустимое кол-во символов для имени - 64.';
          break;

      case 'lastname':
        lastnameValid = value.length <= 64;
        fieldValidationErrors.lastname = lastnameValid ? '': 'Максимально допустимое кол-во символов для фамилии - 64.';
          break;

      case 'patronymic':
        patronymicValid = value.length === 0 || value.length <= 64;
        fieldValidationErrors.patronymic = patronymicValid ? '' : 'Максимально допустимое кол-во символов для отчества - 64.';
          break;

      case 'email':
        emailValid = value.length <= 254;
        fieldValidationErrors.email = emailValid ? '': 'Максимально допустимое кол-во символов для почты - 254.';
          break;

      case 'login':
        loginValid = value.length <= 64;
        fieldValidationErrors.login = loginValid ? '' : 'Максимально допустимое кол-во символов для логина - 64.';
          break;

      case 'password':
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? '': 'Пароль слишком короткий.';
          break;

      case 'passwordRepeat':
        passwordRepeatValid = value === this.state.password;
        fieldValidationErrors.passwordRepeat = passwordRepeatValid ? '': 'Пароли не сходятся.';
          break;

      default:
          break;
    }

    this.setState({formErrors: fieldValidationErrors,
                    emailValid: emailValid,
                    nameValid: nameValid,
                    lastnameValid: lastnameValid,
                    patronymicValid: patronymicValid,
                    loginValid: loginValid,
                    passwordValid: passwordValid,
                    passwordRepeatValid: passwordRepeatValid,
                  }, this.validateForm);
  }

  onUserRegistrationFormInputsChangeHandle(e){

    let name = e.target.name;
    let value = e.target.value.trim();

    this.setState({ [name] : value }, () => { this.validateField(name, value) });
  }

  onUserRegistrationFormSubmitHandler(e){
    e.preventDefault();

    this.setState({ loaderLoaded: false, });

    if(this.state.formValid === true) {

      let data = this.state;

      UserService.create(data)
      .then((response) => {
        this.setState({ redirectToMainPage: true, loaderLoaded: true,});
        alert(response.data.message);
      })
      .catch((error) => {
        this.setState({ loaderLoaded: true, });
        alert("Нет соединения с сервером!");
      });
    }
  }

  render(){
    return(
      <div className = "user-registration-form-wrapper">

        { this.state.redirectToMainPage ? <Redirect exact to = "/" /> : '' }

        <FormErrors formErrors={this.state.formErrors} />
        <form onSubmit = {(e) => this.onUserRegistrationFormSubmitHandler(e)} method = 'POST'>

          <fieldset>
            <label className = "required">
              <input placeholder = 'Имя..' name = 'name' value = {this.state.name} required onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>

            <label className = "required">
              <input placeholder = 'Фамилия..' name = 'lastname' value = {this.state.lastname} required  onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>

            <input placeholder = 'Отчество..' name = 'patronymic' value = {this.state.patronymic} onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>

            <label className = "required">
              <input placeholder = 'Логин..' name = 'login' value = {this.state.login} required onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>

            <label className = "required">
              <input placeholder = 'Почта..' name = 'email' value = {this.state.email} type = 'email' required  onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>
          </fieldset>

          <fieldset>
            <legend>Выберите пол:</legend>
              <label htmlFor = 'sex_m'>М
                <input name = 'sex' value = {this.state.sex} id = 'sex_m' checked = {this.state.sex === SEX.MALE} type = 'radio' value = {SEX.MALE} onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
              </label>
              <label htmlFor = 'sex_f'>Ж
                <input name = 'sex' value = {this.state.sex} id = 'sex_f' checked = {this.state.sex === SEX.FEMALE} type = 'radio' value = {SEX.FEMALE} onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
              </label>
          </fieldset>

          <fieldset>
            <legend>Выберите дату рождения</legend>
            <input name = 'birthday' value = {this.state.birthday} type = 'date' onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
          </fieldset>

          <fieldset>
            <legend>Введите пароль:</legend>
            <label className = "required">
              <input placeholder = 'Пароль..'  name = 'password' value = {this.state.password} type = 'password' required  onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>
            <label className = "required">
              <input placeholder = 'Повторите пароль..' name = 'passwordRepeat' value = {this.state.passwordRepeat} type = 'password' required  onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}/>
            </label>
          </fieldset>

          <fieldset>
            <legend>Выберите город: </legend>
            <select value = {this.state.city} name = 'city' onChange = {(e) => this.onUserRegistrationFormInputsChangeHandle(e)}>
              <option value = {CITIES.MOSCOW}>Москва</option>
              <option value = {CITIES.ST_PETERSBURG}>Санкт-Петербург</option>
              <option value = {CITIES.KURSK}>Курск</option>
            </select>
          </fieldset>
          <div className = 'form-actions'>
            <button type = 'submit' disabled = {!this.state.formValid}>Зарегистрироваться</button>
          </div>
        </form>

        <Loader loaded={this.state.loaderLoaded}/>
      </div>
    )
  }

}
