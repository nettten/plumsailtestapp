import React, { Component } from 'react';
import UserRegistrationPage from './UserRegistrationPage';
import Navigation from './Navigation';

import MainPage from './MainPage';
import { BrowserRouter, Route } from 'react-router-dom';

import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app">

          <Navigation />

          <Route path="/registration" component={UserRegistrationPage} />
          <Route exact path="/" component={MainPage} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
