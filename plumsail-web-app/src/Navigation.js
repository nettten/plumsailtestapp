import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.css';

export default class Navigation extends Component {
  render(){
    return (
      <div>
        <nav>
          <ul>
            <li>
              <NavLink exact = {true} activeClassName = 'active-page' to="/">
                <span title = "Домой" className = "nav-link-img home-img" />
              </NavLink>
            </li>
            <li className = 'registration'>
              <NavLink activeClassName = 'active-page' to="/registration">
                <span title = "Регистрация" className = "nav-link-img registration-img" />
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}
