import React, { Component } from 'react';
import UsersView from './UsersView.js';

import './MainPage.css';

export default class MainPage extends Component {

  componentDidMount(){
    document.title = "Главная страница";
  }

  render(){
    return (
      <div className = 'container'>
          <UsersView pageSizes = {[10, 10, 10, 20, 50]} />
      </div>
    );
  }
}
