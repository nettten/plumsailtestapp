import React, { Component } from 'react';
import UserRegistrationForm from './UserRegistrationForm';

export default class UserRegistrationPage extends Component {

  componentDidMount(){
    document.title = "Регистрация";
  }

  render(){
    return (
      <div className = 'container'>
          <UserRegistrationForm />
      </div>
    );
  }
}
