import React, { Component } from 'react';
import UserService from './api/UserService';
import Loader from 'react-loader';
import './UsersView.css';

export default class UsersView extends Component {

  constructor(props) {
    super(props);

    let pageSizes = [10];
    if(props.pageSizes){
      pageSizes = props.pageSizes.filter((v, i, a) => a.indexOf(v) === i);
    }

    this.state = {
      itemsList: [],
      searchText: '',
      page: 1,
      pageSize: 10,
      pagesCount: 1,
      allItemsCount: 0,
      loaderLoaded: true,
      pageSizes: pageSizes,
    };
  }

  componentDidMount() {
    this.setState({loaderLoaded: false,})
    this.searchUsers(this.state.searchText, this.state.page, this.state.pageSize);
  }

  goToNextPageButtonHandler(e) {
    this.setState({loaderLoaded: false,})

    let nextPage = this.state.page + 1;

    this.searchUsers(this.state.searchText, nextPage, this.state.pageSize);
  }

  goToPrevPageButtonHandler(e) {
    this.setState({loaderLoaded: false,})

    let prevPage = this.state.page - 1;

    this.searchUsers(this.state.searchText, prevPage, this.state.pageSize);
  }

  onSearchInputTextChangeHandler(e) {
    this.setState({loaderLoaded: false,})

    let searchText = e.target.value.trim();

    this.searchUsers(searchText, this.state.page, this.state.pageSize);
  }

  onPageSizeChangeHandler(e) {
    this.setState({loaderLoaded: false,})

    let pageSize = e.target.value;

    this.searchUsers(this.state.searchText, this.state.page, pageSize);
  }

  searchUsers(searchText, page, pageSize){
    return UserService.search(searchText, page, pageSize)
      .then((data) => {

        if(data.response.data.pagesCount === 0){
          data.page = 1;
        }

        this.setState({
          itemsList: data.response.data.itemsList,
          pagesCount: data.response.data.pagesCount,
          allItemsCount: data.response.data.allItemsCount,

          searchText: data.searchText,
          pageSize: data.pageSize,
          page: data.page,
          loaderLoaded: true,
        });

      })
      .catch((err)=>{
        this.setState({ loaderLoaded: true, })
        alert("Нет соединения с сервером!");
      });
  }

  isPageSizesSelectNotAvailable(){
    return this.state.pagesCount === 1;
  }

  isGoToNextPageButtonNotAvailable(){
    return this.state.page === this.state.pagesCount || this.state.pagesCount === 0;
  }

  isGoToPrevPageButtonNotAvailable(){
    return this.state.page === 1;
  }

  renderUsers(){

    if(this.state.itemsList.length === 0){
      return <tr>
              <td colSpan = '100500'>Нет записей..</td>
             </tr>;
    }

    return this.state.itemsList.map((item) => {
      return <tr key = {item.Email}>
              <td>{item.Name}</td>
              <td>{item.LastName}</td>
              <td>{item.Patronymic}</td>
              <td>{item.Login}</td>
              <td>{item.Age}</td>
              <td>{item.Sex}</td>
              <td>{item.City}</td>
             </tr>;
    });
  }

  renderPageSizes(){
    return this.state.pageSizes.map((pageSize) => {
      return <option key = {pageSize}>{pageSize}</option>
    });
  }

  render(){
    return (
      <div>
        <input placeholder = "Поисковый запрос.." type = "search" value = {this.state.searchText} onChange = {e => this.onSearchInputTextChangeHandler(e)}/>
        <table className = "users-view-table">
          <thead>
            <tr>
              <th>Имя</th>
              <th>Фамилия</th>
              <th>Отчество</th>
              <th>Логин</th>
              <th>Возраст</th>
              <th>Пол</th>
              <th>Город</th>
            </tr>
          </thead>
          <tbody>
            {this.renderUsers()}
          </tbody>
        </table>
        <div className = 'pagination'>

          <select disabled = {this.isPageSizesSelectNotAvailable()} style = {{width: '50px'}} value = {this.state.pageSize} onChange = {(e) => this.onPageSizeChangeHandler(e)}>
            { this.renderPageSizes() }
          </select>

          <span className = 'pagination-info'><span className = 'show-text'>Показано с</span> {(this.state.page-1) * this.state.pageSize + 1} по {this.state.page * this.state.pageSize} из {this.state.allItemsCount} <span className = 'records-text'>записей</span></span>

          <button title = "Назад" disabled = {this.isGoToPrevPageButtonNotAvailable()} onClick = {(e) => this.goToPrevPageButtonHandler(e)}>&larr;</button>
          <button title = "Вперед" disabled = {this.isGoToNextPageButtonNotAvailable()} onClick = {(e) => this.goToNextPageButtonHandler(e)}>&rarr;</button>
        </div>

        <Loader loaded={this.state.loaderLoaded}/>
      </div>
    );
  }
}
