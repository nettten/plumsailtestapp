import HttpService from './HttpService.js';
import Config from '../config.json';

let UserService = {

  create: function(user) {
    return HttpService.post(`${Config.serverBaseUri}/api/user`, user);
  },

  search: function(searchText, page, pageSize) {

    searchText = searchText.trim();

    return HttpService.get(`${Config.serverBaseUri}/api/user/?searchText=${searchText}&page=${page}&pageSize=${pageSize}`)
      .then((res) => {
        return {
          searchText: searchText,
          page: page,
          pageSize: pageSize,
          response: res,
        }
      });
  }

};

export default UserService;
