import axios from 'axios';

let HttpService = {
  post: function(url, data){
    return axios.post(url, data);
  },
  get: function(url, data){
    return axios.get(url, data);
  }
};

export default HttpService;
