namespace Plumsail.Core_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_User_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Password", c => c.String(maxLength: 32));
            AlterColumn("dbo.Users", "Patronymic", c => c.String(maxLength: 64));
            AlterColumn("dbo.Users", "Birthday", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Birthday", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "Patronymic", c => c.String());
            DropColumn("dbo.Users", "Password");
        }
    }
}
