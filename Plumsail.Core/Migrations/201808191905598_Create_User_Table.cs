namespace Plumsail.Core_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create_User_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 254),
                        Login = c.String(maxLength: 64),
                        Name = c.String(maxLength: 64),
                        LastName = c.String(maxLength: 64),
                        Patronymic = c.String(),
                        City = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Email);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
