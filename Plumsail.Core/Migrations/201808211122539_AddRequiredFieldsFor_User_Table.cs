namespace Plumsail.Core_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredFieldsFor_User_Table : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "LastName", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 32));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Password", c => c.String(maxLength: 32));
            AlterColumn("dbo.Users", "LastName", c => c.String(maxLength: 64));
            AlterColumn("dbo.Users", "Name", c => c.String(maxLength: 64));
            AlterColumn("dbo.Users", "Login", c => c.String(maxLength: 64));
        }
    }
}
