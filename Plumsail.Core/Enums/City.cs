﻿using System.ComponentModel.DataAnnotations;

namespace Plumsail.Core.Enums
{
    public enum City
    {
        [Display(Name = "Москва")]
        Moscow = 777,

        [Display(Name = "Санкт-Петербург")]
        StPetersburg = 78,

        [Display(Name = "Курск")]
        Kursk = 46,
    }
}
