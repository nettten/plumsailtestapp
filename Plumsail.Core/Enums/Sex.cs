﻿using System.ComponentModel.DataAnnotations;

namespace Plumsail.Core.Enums
{
    public enum Sex
    {
        [Display(Name = "M")]
        Male = 0,
        [Display(Name = "Ж")]
        Female = 1,
    }
}
