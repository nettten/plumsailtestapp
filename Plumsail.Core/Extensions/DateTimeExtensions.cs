﻿using System;

namespace Plumsail.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static int GetDifferentInYears(this DateTime start, DateTime end)
        {
            int yearsDifferent = end.Year - start.Year;

            if (start.Month == end.Month && end.Day < start.Day || end.Month < start.Month)
            {
                yearsDifferent--;
            }

            return yearsDifferent;
        }

    }
}
