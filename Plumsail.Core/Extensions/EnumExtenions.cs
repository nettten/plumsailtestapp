﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Plumsail.Core.Extensions
{
    public static class EnumExtenions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            var enumType = enumValue.GetType();

            var enumValueString = enumValue.ToString();
            var memberInfo = enumType.GetField(enumValueString);

            if (memberInfo != null)
            {
                var dispayAttribute = memberInfo.GetCustomAttribute<DisplayAttribute>(false);

                if (dispayAttribute != null)
                {
                    return dispayAttribute.Name;
                }
            }

            return string.Empty;
        }
    }
}
