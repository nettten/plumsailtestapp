﻿using Plumsail.Core.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Plumsail.Core.DAL.Models
{
    public class User
    {
        [Key, StringLength(254)]
        public string Email { get; set; }

        [Required]
        [StringLength(64)]
        public string Login { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [Required]
        [StringLength(64)]
        public string LastName { get; set; }

        [StringLength(64)]
        public string Patronymic { get; set; }

        [Required]
        [StringLength(32)]
        public string Password { get; set; }

        public City City { get; set; }
        public Sex Sex { get; set; }

        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; }
        
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            User user = obj as User;

            return user.Email.Equals(Email);
        }
        
        public override int GetHashCode()
        {
            return Email.GetHashCode() * 7;
        }
    }
}
