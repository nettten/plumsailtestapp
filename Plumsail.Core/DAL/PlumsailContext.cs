﻿using Plumsail.Core.DAL.Models;
using System.Data.Entity;

namespace Plumsail.Core.DAL
{
    public class PlumsailContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}
