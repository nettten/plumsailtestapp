﻿using AutoMapper;
using Plumsail.Core.DAL.Models;
using Plumsail.Core.Extensions;
using Plumsail.Web.Models;
using Plumsail.Web.Services.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace Plumsail.Web.Services
{
    public class UserService : IDisposable
    {
        readonly DbContext _dbContext;
        readonly IMapper _mapper;

        public UserService(DbContext dbContext)
        {
            _dbContext = dbContext;

            _mapper = new MapperConfiguration(c =>
            {
                c.CreateMap<UserModel, User>()
                    .ForMember(d => d.City, s => s.MapFrom(sd => sd.CityValue))
                    .ForMember(d => d.Sex, s => s.MapFrom(sd => sd.SexValue))
                    .ForMember(d => d.Password, s => s.MapFrom(sd => sd.HashedPassword));

                c.CreateMap<User, UserView>()
                    .ForMember(d => d.Age, s => s.MapFrom(sd => sd.Birthday.GetDifferentInYears(DateTime.Today)))
                    .ForMember(d => d.City, s => s.MapFrom(sd => sd.City.GetDisplayName()))
                    .ForMember(d => d.Sex, s => s.MapFrom(sd => sd.Sex.GetDisplayName()));

            }).CreateMapper();
        }

        public void CreateUser(UserModel userModel)
        {
            if (userModel == null)
            {
                throw new ArgumentNullException(nameof(userModel));
            }

            var isExistUserWithSameEmail = _dbContext.Set<User>()
                                            .Any(u => u.Email.Equals(userModel.Email));

            if (isExistUserWithSameEmail)
            {
                throw new ArgumentException("Пользователь с такой почтой уже существует в системе!");
            }

            var user = _mapper.Map<User>(userModel);

            _dbContext.Set<User>().Add(user);
            _dbContext.SaveChanges();
        }

        public UsersSearchResult GetUsersBy(SearchRequestModel searchRequestModel)
        {
            var allUsers = _dbContext.Set<User>().ToList();
            
            var page = searchRequestModel.Page;
            var pageSize = searchRequestModel.PageSize;

            if (!string.IsNullOrWhiteSpace(searchRequestModel.SearchText))
            {
                allUsers = FilteringUsersBy(allUsers, searchRequestModel.SearchText);
            }
            
            var users = allUsers
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var usersView = _mapper.Map<List<UserView>>(users);
            var allItemsCount = allUsers.Count();
            
            return new UsersSearchResult
            {
                AllItemsCount = allItemsCount,
                PagesCount = allItemsCount / pageSize,
                Users = new ReadOnlyCollection<UserView>(usersView),
            };
        }

        private List<User> FilteringUsersBy(List<User> users, string searchText)
        {
            return users.Where(u => u.Name.ToLower().Contains(searchText) ||
                            u.LastName.ToLower().Contains(searchText) ||
                            u.Patronymic.ToLower().Contains(searchText) ||
                            u.Login.ToLower().Contains(searchText) ||
                            u.City.GetDisplayName().ToLower().Contains(searchText) ||
                            u.Sex.GetDisplayName().ToLower().Contains(searchText)).ToList();
        }

        public void Dispose()
        {
            if(_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}