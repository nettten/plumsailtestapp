﻿using Plumsail.Web.Models;
using System.Collections.Generic;

namespace Plumsail.Web.Services.Models
{
    public class UsersSearchResult
    {
        public IReadOnlyList<UserView> Users { get; set; }
        public int AllItemsCount { get; set; }
        public int PagesCount { get; set; }
    }
}