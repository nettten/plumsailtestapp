﻿using Plumsail.Core.Enums;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Plumsail.Web.Models
{
    public class UserModel
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }

        public string Login { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime Birthday { get; set; }
        public string City { get; set; }
        public string Sex { get; set; }

        public Sex SexValue
        {
            get
            {
                if(Enum.TryParse(Sex, out Sex sex))
                {
                    return sex;
                }

                throw new ArgumentException("Город не определен!!");
            }
        }

        public City CityValue
        {
            get
            {
                if(Enum.TryParse(City, out City city))
                {
                    return city;
                }

                throw new ArgumentException("Город не определен!!");
            }
        }

        public string HashedPassword
        {
            get
            {
                using (var md5 = MD5.Create())
                {

                    var passwordBytes = Encoding.Unicode.GetBytes(Password);
                    var hashedBytes = md5.ComputeHash(passwordBytes);

                    var passwordHashedString = new StringBuilder();

                    for (int i = 0; i < hashedBytes.Length; i++)
                    {
                        passwordHashedString.Append(hashedBytes[i].ToString("X2"));
                    }

                    return passwordHashedString.ToString();

                }
            }
        }

    }
}