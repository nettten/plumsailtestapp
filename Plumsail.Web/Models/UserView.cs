﻿using System;

namespace Plumsail.Web.Models
{
    public class UserView
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }

        public string Login { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string Sex { get; set; }
    }
}