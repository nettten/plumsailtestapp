﻿namespace Plumsail.Web.Models
{
    public class SearchRequestModel
    {
        private string _searchText;

        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value?.Trim().ToLower();
            }
        }
        
        public int Page { get; set; }
        
        public int PageSize { get; set; }
    }
}