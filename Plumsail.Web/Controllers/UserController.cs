﻿using Plumsail.Core.DAL;
using Plumsail.Web.Models;
using Plumsail.Web.Services;
using System;
using System.Data;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Plumsail.Web.Controllers
{
    [EnableCors("*", "*", "*")]
    public class UserController : ApiController
    {
        UserService _userService;

        public UserController()
        {
            _userService = new UserService(new PlumsailContext());
        }

        [HttpPost]
        public IHttpActionResult Create(UserModel userModel)
        {
            try
            {
                _userService.CreateUser(userModel);
            }
            catch (ArgumentException e)
            {
                return Json(new
                {
                    success = false,
                    message = e.Message,
                });
            }
            catch (DataException)
            {
                return Json(new
                {
                    success = false,
                    message = "Произошла ошибка при попытке регистрации!",
                });
            }

            return Json(new
            {
                success = true,
                message = "Регистрация прошла успешно!",
            });
        }

        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchRequestModel searchRequestModel)
        {
            var usersSearchResult =_userService.GetUsersBy(searchRequestModel);

            return Json(new
            {
                allItemsCount = usersSearchResult.AllItemsCount,
                pagesCount = usersSearchResult.PagesCount,
                itemsList = usersSearchResult.Users,
            });
        }

        protected override void Dispose(bool disposing)
        {
            if(_userService != null)
            {
                _userService.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
